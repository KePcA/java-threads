
public class Test {
	public static void main(String[] args) {
		TestThread t1, t2, t3;
		t1 = new TestThread("Thread 1");
		t1.start();
		t2 = new TestThread("Thread 2");
		t2.start();
		t3 = new TestThread("Thread 3");
		t3.start();
	}
}





class TestThread extends Thread {
	
	String id;
	
	public TestThread(String s) {
		id = s;
	}
	
	public void doCalc(int i) {
		//Perform complex calculation based on i.
	}
	
	public void run() {
		int i;
		for(i=0; i<10; i++) {
			doCalc(i);
			System.out.println(id);
		}
	}	
}
