
public class CircularList {
	
	private CircularListNode current;
	
	public synchronized void insert(Object o) {
		CircularListNode tn = new CircularListNode();
		tn.o = o;
		//No element in a list
		if(current == null) {
			tn.next = tn.prev = tn;
			current = tn;
		}
		//Add before current node
		else {
			tn.next = current;
			tn.prev = current.prev;
			current.prev.next = tn;
			current.prev = tn;
		}
	}
	
	
	public synchronized void delete(Object o) {
		CircularListNode p = find(o);
		CircularListNode next = p.next;
		CircularListNode prev = p.prev;
		//Last object in a list
		if(p == p.next) {
			current = null;
			return;
		}
		prev.next = next;
		next.prev = prev;
		if(current == p) {
			current = next;
		}
	}
	
	
	public CircularListNode find(Object o) {
		CircularListNode p = current;
		if(p == null) {
			throw new IllegalArgumentException();
		}
		do {
			if(p.o == o) { return p; }
			p = p.next;
		} while(p != current);
		throw new IllegalArgumentException();
	}
	
	
	public synchronized Object locate(Object o) {
		CircularListNode p = current;
		do {
			if(p.o.equals(o)) { return p.o; }
			p = p.next;
		} while(p != current);
		throw new IllegalArgumentException();
	}
	
	
	public synchronized Object getNext() {
		if(current == null) {
			return null;
		}
		current = current.next;
		return current.o;
	}
}
