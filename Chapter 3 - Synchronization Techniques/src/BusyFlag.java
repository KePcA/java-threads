//Class for setting a flag when the thread grabs the lock
public class BusyFlag {
	
	protected Thread busyflag = null;
	protected int busycount = 0;
	
	
	//Waits in a loop until it gets a flag
	public void getBusyFlag() {
		while(tryGetBusyFlag() == false) {
			try {
				Thread.sleep(100);
			}
			catch (Exception e) { }
		}
	}
	
	//Tries to grab a look. Is the flag is free it grabs it. Sets the counter 
	//to 1 since it's the first time it grabs a look.
	public synchronized boolean tryGetBusyFlag() {
		if(busyflag == null) {
			busyflag = Thread.currentThread();
			busycount = 1;
			return true;
		}
		if(busyflag == Thread.currentThread()) {
			busycount++;
			return true;
		}
		return false;
	}
	
	//Frees the flag. Does this when counter hits 0
	public synchronized void freeBusyFlag() {
		if(getBusyFlagOwner() == Thread.currentThread()) {
			busycount--;
			if(busycount == 0) {
				busyflag = null;
			}
		}
	}
	
	//Returns the thread which is currently holding a lock.
	public synchronized Thread getBusyFlagOwner() {
		return busyflag;
	}

}
