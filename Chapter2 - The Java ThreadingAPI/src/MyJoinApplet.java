import java.applet.Applet;

//Example of joining multiple threads.
public class MyJoinApplet extends Applet {
	
	Thread[] t = new Thread[30];
	
	public void start() {
		for(int i=0; i<30; i++) {
			t[i] = new CalcThread(i);
			t[i].start();
		}
	}
	
	//Loop of the join method which waits until all trades have finished - 
	//perfectly ok solution.
	public void stop() {
		for(int i=0; i<30; i++) {
			try {
				t[i].join();
			}
			catch (InterruptedException e) { }
		}
	}
	
}





//A test class like a part of a large math algorithm for example. 
class CalcThread extends Thread {
	
	private int number;
	
	public CalcThread(int number) {
		this.number = number;
	}
	
	public void run() {
		//Some calculations.
	}
	
}