
public class ResourceThrottle {
	
	private int resourcecount = 0;
	private int resourcemax = 1;
	
	public ResourceThrottle(int max) {
		resourcecount = 0;
		resourcemax = max;
	}
	
	//Tries to add some resource. If it cant it waits until some of them are freed
	public synchronized void getResource(int numberOf) {
		while(true) {
			if((resourcecount + numberOf) <= resourcemax) {
				resourcecount += numberOf;
				break;
			}
			try {
				wait();
			}
			catch(Exception e) { }
		}
	}
	
	//Frees the amount of resources. Notifies that some of the resources were freed.
	public synchronized void freeResource(int numberOf) {
		resourcecount -= numberOf;
		notifyAll();
	}

}
