
public class TargetNotify {
	
	private Object[] Targets = null;
	
	public TargetNotify(int numberOfTargets) {
		Targets = new Object[numberOfTargets];
		
		for(int i=0; i<numberOfTargets; i++) {
			Targets[i] = new Object();
		}
	}
	
	//Calls wait method for the selected object.
	public void wait(int targetNumber) {
		synchronized(Targets[targetNumber]) {
			try {
				Targets[targetNumber].wait();
			}
			catch(Exception e) { }
		}
	}
	
	//Send notify signal to the selected object.
	public void notify(int targetNumber) {
		synchronized(Targets[targetNumber]) {
			Targets[targetNumber].notify();
		}
	}

}
