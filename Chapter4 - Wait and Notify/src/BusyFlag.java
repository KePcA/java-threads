
public class BusyFlag {
	
	protected Thread busyflag = null;
	protected int busycount = 0;
	
	//Tries to get a busy flag. Waits for the notify to occur.
	public synchronized void getBusyFlag() {
		while(tryGetBusyFlag() == false) {
			try {
				wait();
			}
			catch (Exception e) { }
		}
	}
	
	//Grabs a flag if it is available 
	public synchronized boolean tryGetBusyFlag() {
		if(busyflag == null) {
			busyflag = Thread.currentThread();
			busycount = 1;
			return true;
		}
		if(busyflag == Thread.currentThread()) {
			busycount++;
			return true;
		}
		return false;
	}
	
	//Frees the flag and notifies that it's free to use.
	public synchronized void freeBusyFlag() {
		if(getBusyFlagOwner() == Thread.currentThread()) {
			busycount--;
			if(busycount == 0) {
				busyflag = null;
				notify();
			}
		}
	}
	
	public synchronized Thread getBusyFlagOwner() {
		return busyflag;
	}
	
}
